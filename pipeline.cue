package pipeline

import (
    "dagger.io/dagger"
    "universe.dagger.io/docker"
)

#StagingToProductionDeploy: docker.#Dockerfile & {
    dockerfile: contents: """
        FROM python:3.9
        COPY . /src
        RUN pip install -r /src/requirements.txt
        CMD python /src/staging_to_production_deploy.py
        """
}

dagger.#Plan & {
	client: {
		filesystem: "./src": read: contents: dagger.#FS
		env: {
			GOODDATA_HOST: string
			GOODDATA_API_TOKEN: string
			GOODDATA_STAGING_WORKSPACE_ID: string
			GOODDATA_PRODUCTION_WORKSPACE_ID: string
		}
	}

	actions: {
		build: #StagingToProductionDeploy & {
			source: client.filesystem."./src".read.contents
		}

		run: docker.#Run & {
			always: true
			input: build.output
			env: {
				GOODDATA_HOST: client.env.GOODDATA_HOST
    		GOODDATA_API_TOKEN: client.env.GOODDATA_API_TOKEN
    		GOODDATA_STAGING_WORKSPACE_ID: client.env.GOODDATA_STAGING_WORKSPACE_ID
    		GOODDATA_PRODUCTION_WORKSPACE_ID: client.env.GOODDATA_PRODUCTION_WORKSPACE_ID
			}
		}
	}
}
