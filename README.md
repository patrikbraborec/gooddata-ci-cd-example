# Gooddata Ci Cd Example

An example of how to use GoodData Python SDK for CI/CD.

--

The repository is a demo created for the article: [How to Automate Data Analytics Using CI/CD](https://medium.com/gooddata-developers/how-to-automate-data-analytics-using-ci-cd-9f1475065d61).
